package stocka;

import java.awt.EventQueue;
import java.util.ArrayList;

/**
 * 
 * @author Manfred, Ismail, Saad, Charles
 *
 */
public class Main {

	public static void main(String[] args) {

		Parseur p = new Parseur();
		Scenario sc = new Scenario(15, 1, new Parseur().getStations());
		sc.init_modele();
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					UserInterface window = new UserInterface(sc.getStations());
					window.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
	public static Scenario test1(int c,int v,int w) {
		ArrayList<Station> a = new Parseur().getStations();
		for (Station s : a) {
			s.setC(c);
			s.setV(v);
			s.setW(w);
		}
		Scenario sc = new Scenario(15, 1, a);
		sc.init_modele();
		RecuitDeterministe recuit = new RecuitDeterministe(sc);

		Scenario res = recuit.recuit_simule();
		System.out.println("Fonction ObjectiveI : "+sc.fonction_objective());
		System.out.println("Fonction ObjectiveR : "+res.fonction_objective());
		System.out.println(res.verification_contraintes());

		return res;
	}


	
	public static void testSAA(int c, int v, int w){
		ArrayList<Station> a = new Parseur().getStations();
		for (Station s : a) {
			s.setC(c);
			s.setV(v);
			s.setW(w);
		}
		Scenario sc = new Scenario(15, 1, a);
		sc.init_modele();
		SAA saa = new SAA(2, 2, sc);
		System.out.println("\t---- Solution de l'algorithme SAA ----\n");
		saa.getS_best().affichage();
	}
	
	
	/**
	 * Test du recuit deterministe avec 3 stations seulement
	 * 
	 * @return
	 */
	public static Scenario testRecuitDeterministe(int c, int v, int w) {
		Scenario sc = miniScenario();
		for (Station s : sc.getStations()) {
			s.setC(c);
			s.setV(v);
			s.setW(w);
		}
		sc.calcul_beta_global();
		RecuitDeterministe recuit = new RecuitDeterministe(sc);
		
		Scenario res = recuit.recuit_simule();
		System.out.println("Fonction ObjectiveI : "+sc.fonction_objective());
		System.out.println("Fonction ObjectiveR : "+res.fonction_objective());
		System.out.println(res.verification_contraintes());
		return res;
	}
	
	/**
	 * Creation d'un scenario simple
	 * @return
	 */
	public static Scenario miniScenario(){
		ArrayList<Station> st = new ArrayList<Station>();
		Station s;
		int[] k = { 40, 10, 30 };
		int[] x = { 15, 10, 8 };
		int[][] ksi = { { 10, 5 }, { 6, 11 }, { 8, 12 } };
		
		//initialisation du minitest
		System.out.println("\n\t---- Initialisation du minitest ----\n");
		for (int i = 0; i < 3; i++) {
			s = new Station();
			s.setName("Station "+i);
			s.setNumber(i);
			s.setK(k[i]);
			s.setX(x[i]);
			s.setNumber(i);
			int count = 0;
			for (int j = 0; j < 2; j++) {
				if (i == count)
					count += 1;

				s.setKsi(count, ksi[i][j]);
				count++;
			}
			st.add(s);
			st.get(i).affichage();
		}
		
		return new Scenario(500, 1, st);
	}
	

	public static void test_miniSAA(){
		Scenario sc = miniScenario();
		sc.calcul_beta_global();
		SAA saa = new SAA(2, 2, sc);
		System.out.println("\t---- Solution de l'algorithme SAA ----\n");
		saa.getS_best().affichage();
	}
}

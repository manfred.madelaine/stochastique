package stocka;

import java.util.Scanner;

/**
 * 
 * @author Manfred, Ismail, Saad, Charles
 *
 */
public class RecuitStochastique extends RecuitGenerique{
	private Echantillon echantillon;
	public static int COEF_DE_PENALITE = 2;
	
	
    /**
     * Constructeur de la classe RecuitStochastique
     * @param e : echantillon de scenarios
     */
	public RecuitStochastique (Echantillon e) {
		echantillon = e;
		recuitStochastique();
	}
      
	
	/**
	 * Fonction permettant calculer le recuit stochastique
	 */
	public void recuitStochastique(){

		System.out.println("\n\t---- Recuit Stochastique ----\n");

		Scenario[] resultats = recuit_simule();
		
		boucle(scenarioMoyen(resultats));
	}
	

	/**
	 * Renvoie le meilleur scenario parmi les scenarios de l'echantillon
	 * @return : le meilleur scenario
	 */
 	public static Scenario getBestScenario(Echantillon echantillon){
		Scenario best_sc = echantillon.getScenarios()[0];
		for (Scenario s : echantillon.getScenarios()){
			if(best_sc.fonction_objective() > s.fonction_objective())
				best_sc = s;
		}
		return best_sc;
	}
 	
 	
	/**
	 * Fait appel au recuit simule sur l'ensemble des scenarios de l'echantillon et 
	 * stock chacuns des scenarios renvoyes par le recuit dans un tableau de resultat
	 * @return un tableau contenant les meilleurs solutions pour chacun des scenarios de l'echantillon
	 */
	public Scenario [] recuit_simule() {
		Scenario[] resultats = new Scenario[echantillon.getScenarios().length];
		
		//parcours les scenarios de l'echantillon
		for (int i = 0; i < echantillon.getScenarios().length; i++) {
			resultats[i] = super.recuit_simule(echantillon.getScenarios()[i]);
		}
		
		return resultats;
	} 
        
	
	/**
	 * Renvoie la moyenne des velos affectes pour chacunes des stations sur l'ensemble des scenarios de l'echantillon
	 * @param tab_sc : Tableau de scenarios sur lequel on calcul la moyenne de velos par station
	 * @return un tableau d'entier de la taille du nombre de station dans un scenario, contenant la moyenne des velos
	 * 			attribues a chacunes des stations sur l'ensemble des scenarios
	 */
	public int[] scenarioMoyen(Scenario[] tab_sc){
		System.out.println("\n\t---- Scenario Moyen ----\n");
		
		int[] x = new int [tab_sc[0].getStations().size()];
				
		for (Scenario s : tab_sc){
			for(int i = 0; i < s.getStations().size(); i++){
				x[i] += s.getStations().get(i).getX();
			}
		}
		
		for(int i = 0; i < x.length; i++){
			x[i] /= tab_sc.length;
//			System.out.println("Moyenne station_" + i +" = "+ x[i]);
		}
		return x;
	}
	
	
	/**
	 * Fonction bouclant sur le recuit simule tant que la condition de sortie n'est pas verifie 
	 * 		(condition definie dans la fonction "finBoucle")
	 * @param moyenne : tableau d'entier correspondant a la valeur moyenne de velos disponibles dans chacunes des stations
	 */
	public void boucle(int[] moyenne){
		Scenario[] resultats = new Scenario[echantillon.getScenarios().length];
		boolean b = false;
		while(!b){
			for (int i = 0; i < echantillon.getScenarios().length; i++) {
				resultats[i] = recuit_simule2(echantillon.getScenarios()[i], moyenne);
			}
			
			moyenne = scenarioMoyen(resultats);
			b = finBoucle(echantillon.getScenarios());
			
			//TODO a suprimer
			new Scanner(System.in).nextLine();
		}
		
	}
	
	
	/**
	 * Condition de sortie de la boucle sur le recuit simule
	 * @param tab_sc : tableau de scenarios 
	 * @return true si on peut sortir de la boucle, false sinon
	 */
	public boolean finBoucle(Scenario[] tab_sc){
		for (int i = 1; i < echantillon.getScenarios().length; i++) {
			for(int j = 0; j < echantillon.getScenarios()[i].getStations().size(); j++){
				if (echantillon.getScenarios()[i].getStations().get(j).getX() != echantillon.getScenarios()[i-1].getStations().get(j).getX())
					return false;
			}
		}
		return true;
	}

	
	/**
	 * Fonction calculant le recuit simule en utilisant une autre fonction du calcul de fo
	 * @param s : scenario de reference
	 * @param moyenne : tableau de valeurs de X pour chacune des stations
	 * @return le meilleur scenario du recuit simule
	 */
	public Scenario recuit_simule2(Scenario s, int[] moyenne) {
		
		System.out.println("\n\t---- Debut du recuit Simule 2 : fo = " + s.fonction_objective() + " ----\n");
		
		Scenario x = s;
		Scenario x_meilleur = s;
		int accept = 0;
		double temperature = s.getTemperature();
		int nb_iterations = 0;
		int n = 5;
		double p;
		double delta = 0;
		int stop = 5;
		boolean cycle = true;
		while (cycle) {
			stop--;
			cycle = false;
			nb_iterations = 0;
			accept = 0;
			while (nb_iterations < n) {
				System.out.println("\n\t\titeration : " + nb_iterations);
				nb_iterations++;
				Scenario x_voisin = voisinage(x);
				delta = fo(x_voisin, moyenne) - fo(x, moyenne);
				if (delta < 0) {
//					System.out.println("on a une meilleure solution.");
					x = x_voisin;
					cycle = true;
					accept++;
					if (fo(x, moyenne) < fo(x_meilleur, moyenne)){
						x_meilleur = x;
//						System.out.println("fo_best = " + x_meilleur.fonction_objective());
					}
				} else {
//					System.out.println("delta = " + delta);
//					System.out.println("Pas d'améliration (on change ou on reste ?)");
					p = Math.random();
					System.out.println("on tire une valeur aléatoirement.");
					if (p < Math.exp(delta / temperature)) {
						System.out.println("on garde.");
						x = x_voisin;
						cycle = true;
						accept++;
					}
				}
			}
			if ((accept / nb_iterations) < 0.8){
				System.out.println("La T° ne laisse pas passer assez de voisinage: old T = " + temperature + "\tnew T = " + temperature*2);
				temperature = temperature * 2;
			}
			else{
				temperature = temperature * 0.8;
				
			}
			if (stop == 0)
				cycle = false;
		}
		return x_meilleur;
	}
	
	
	/**
	 * Calcul de la fonction objectif prenant en compte une penalite si le nombre de velos aloues a une station 
	 * est eloigne de la moyenne
	 * @param x : scenario sur lequel on calcul la fonction objectif
	 * @param moyenne : tableau de valeurs de X pour chacune des stations
	 * @return la valeur de la fonction objectif plus la penalite
	 */
	public double fo(Scenario x, int[] moyenne){
		int penalite = 0;
		
		for (int i = 0; i < x.getStations().size(); i++){
			if(x.getStations().get(i).getX() != moyenne[i])
				penalite += COEF_DE_PENALITE*Math.abs(x.getStations() .get(i).getX() - moyenne[i]);
		}
		return (x.fonction_objective() + penalite);
	}
	

	public Echantillon getEchantillon() {
		return echantillon;
	}

	public void setEchantillon(Echantillon echantillon) {
		this.echantillon = echantillon;
	}

}
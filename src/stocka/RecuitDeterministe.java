package stocka;

/**
 * 
 * @author Manfred, Ismail, Saad, Charles
 *
 */
public class RecuitDeterministe extends RecuitGenerique {
	private Scenario scenario;

	public RecuitDeterministe(Scenario s) {
		scenario = s;
	}

	/**
	 * Appel au recuit simule de la classe mere
	 * 
	 * @return le meilleur scenario du recuit simule
	 */
	public Scenario recuit_simule() {
		return super.recuit_simule(scenario);
	}

	public Scenario getS() {
		return scenario;
	}

	public void setS(Scenario s) {
		scenario = s;
	}

}
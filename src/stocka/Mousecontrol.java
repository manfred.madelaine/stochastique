package stocka;

import java.awt.*;
import java.awt.event.*;

import javax.swing.event.*;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

public class Mousecontrol implements MouseListener, MouseMotionListener {

	RenderCanvas pan;
	Station d, s;
	int x, y;

	public Mousecontrol(RenderCanvas p) {
		this.pan = p;
	}


	public void mouseClicked(MouseEvent e) {
	}

	public void mouseDragged(MouseEvent e) {
		if (s == null) {
			for (Station ss : pan.getStations()) {
				ss.setLat(ss.getLat()
						+ (100 * (e.getX() / pan.getZoom())) - x);
				ss.setLng(ss.getLng()
						+ (100 * (e.getY() / pan.getZoom())) - y);
				pan.repaint();
			}
			x = 100 * (e.getX() / pan.getZoom());
			y = 100 * (e.getY() / pan.getZoom());
		}
		pan.repaint();
	}
	public double distance (double x1, double y1, double x2, double y2) {
		return Math.sqrt(((x2 - x1)*(x2 - x1)) + 
				((y2 - y1)*(y2 - y1)));
	}
	public void mouseReleased(MouseEvent e) {
		if (e.getClickCount() == 2)
			pan.setZoom(pan.getZoom()+20);
		if (SwingUtilities.isRightMouseButton(e) && pan.getZoom() > 20)
			pan.setZoom(pan.getZoom()-20);
		pan.repaint();
	}
	
	public boolean isAround(int a, int b, int xx, int yy) {
		return (a >= (xx - 5) && a <= (xx + 5) &&
				b >= (yy - 5) && b <= (yy + 5)); 
	}

	
	public void mousePressed(MouseEvent e) {
		x = (100 * e.getX() / pan.getZoom());
		y = (100 * e.getY() / pan.getZoom());
		for (Station ss : pan.getStations()) {
			if (isAround(100 * e.getX() / pan.getZoom(), 100 * e.getY()
					/ pan.getZoom(), (int)ss.getLat(), (int)ss.getLng())
					&& e.getButton() == 1) {
				if (s == null) {
					s = ss;
					ss.setDisplay(true);
					pan.repaint();
				} else {
					if (s == ss) {
						s = null;
						ss.setDisplay(false);
					} else {
						s = ss;
						for (Station stat : pan.getStations()) {
							stat.setDisplay(false);
						}
						ss.setDisplay(true);
					}
				}
				break;
			}
		}
		pan.repaint();
	}

	public void mouseEntered(MouseEvent e) {
	}

	public void mouseExited(MouseEvent e) {
	}

	public void mouseMoved(MouseEvent e) {
	}
}
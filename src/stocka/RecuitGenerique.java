package stocka;

import java.util.ArrayList;
import java.util.Random;

/**
 * 
 * @author Manfred, Ismail, Saad, Charles
 *
 */
class RecuitGenerique {

	/**
	 * Calcule un scenario voisin a partir d'un scenario passe en parametre
	 * @param s : scenario de reference
	 * @return scenario voisin
	 */
	public Scenario voisinage(Scenario s) {		
		Scenario cp = (Scenario) s.clone();
		ArrayList<Station> stations = cp.getStations();
		int t = stations.size();
		Random r = new Random();
		int x1 = 0, x2 = 0;

		//tant que la station x1 n'a pas de velo dispo ou que x2 n'a pas de place pour acceuillir de nouveau velo
		while (x1 == x2  || stations.get(x1).getX() == 0 || stations.get(x2).getABS() == 0) {
			x1 = r.nextInt(t);
			x2 = r.nextInt(t);
		}
		int nb_velos = 0;
		do {
				nb_velos = 1 + r.nextInt(stations.get(x1).getX());
		} while ((nb_velos > stations.get(x2).getABS()) || (stations.get(x1).getX() - nb_velos < 0));
				
		//transfert de nb_velos de la station  x1 a la station x2
		stations.get(x1).setX(stations.get(x1).getX() - nb_velos);
		stations.get(x2).setX(stations.get(x2).getX() + nb_velos);
		
		//mise a jour du modele
		cp.calcule_modele(stations.get(x1), stations.get(x2));
		return cp;
	}
	

	/**
	 * Fonction effectuant un recuit simule sur un scenario passe en parametre
	 * @param s : scenario de reference
	 * @return le meilleur scenario du recuit simule
	 */
	public Scenario recuit_simule(Scenario s) {
		System.out.println("\n\t---- Debut du recuit Simule : fo = " + s.fonction_objective() + " ----\n");
		Scenario x = s;
		Scenario x_meilleur = s;
		int accept = 0;
		double temperature = s.getTemperature();
		int nb_iterations = 0;
		int n = 10;
		double p;
		double delta = 0;
		int stop = 5;
		boolean cycle = true;
		while (cycle) {
			stop--;
			cycle = false;
			nb_iterations = 0;
			accept = 0;
			while (nb_iterations < n) {
				nb_iterations++;
				Scenario x_voisin = voisinage(x);
				delta = x_voisin.fonction_objective() - x.fonction_objective();
				if (delta < 0) {
					x = x_voisin;
					cycle = true;
					accept++;
					if (x.fonction_objective() < x_meilleur.fonction_objective())
						x_meilleur = x;
				} else {
					p = Math.random();
					if (p <= Math.exp(-(delta / temperature))) {
						x = x_voisin;
						cycle = true;
						accept++;
					}
				}
			}
			if ((accept / nb_iterations) < 0.8)
					temperature = temperature * 2;
			else
				temperature = temperature * 0.8;
			
			if (stop == 0)
				cycle = false;
		}
		return x_meilleur;
	}

}
package stocka;

import java.sql.Timestamp;
import java.util.HashMap;

/**
 * 
 * @author Manfred, Ismail, Saad, Charles
 *
 */
public class Station implements Cloneable {

	private int number;
	private boolean display = false;
	private String name;
	private String adress;
	private double latitude;
	private double longitude;
	private boolean banking;
	private boolean bonus;
	private String status;
	private String contract;
	private int k;
	private int abs; //available bike stands
	private HashMap<Integer, Integer> ksi;
	private int x;
	private Timestamp ts;
	private int c;
	private int v;
	private int w;
	private int omoins;
	private int oplus;
	private int iplus;
	private HashMap<Integer, Integer> beta;
	private HashMap<Integer, Integer> imoins;

	public Station() {
		c = 3;
		v = 4;
		w = 7;
		ksi = new HashMap<Integer, Integer>();
		beta = new HashMap<Integer, Integer>();
		imoins = new HashMap<Integer, Integer>();
	}

	@SuppressWarnings("unchecked")
	public Object clone() {
		Object o = null;
		try {
			o = super.clone();
		} catch (CloneNotSupportedException cnse) {
			cnse.printStackTrace(System.err);
		}
		((Station) o).ksi = (HashMap<Integer, Integer>) ksi.clone();
		((Station) o).beta = (HashMap<Integer, Integer>) beta.clone();
		((Station) o).imoins = (HashMap<Integer, Integer>) imoins.clone();
		return o;
	}

	/*
	 * Fonctions permettant de recuperer la valeur des parametres de la station
	 */
	public int getNumber() {
		return number;
	}

	public String getName() {
		return name;
	}
	
	public boolean getDisplay() {
		return display;
	}

	public String getAdress() {
		return adress;
	}

	public double getLat() {
		return latitude;
	}

	public double getLng() {
		return longitude;
	}

	public boolean getBanking() {
		return banking;
	}

	public boolean getBonus() {
		return bonus;
	}

	public String getStatus() {
		return status;
	}

	public String getContract() {
		return contract;
	}

	public int getK() {
		return k;
	}

	public int getKsi(int i) {
		return ksi.get(i);
	}

	public HashMap<Integer, Integer> getKsi() {
		return ksi;
	}

	public HashMap<Integer, Integer> getBeta() {
		return beta;
	}

	public int getBeta(int i) {
		return beta.get(i);
	}

	public HashMap<Integer, Integer> getImoins() {
		return imoins;
	}

	public int getImoins(int i) {
		return imoins.get(i);
	}

	public int getX() {
		return x;
	}

	public int getABS() {
		return abs;
	}

	public Timestamp getTS() {
		return ts;
	}

	public int getOmoins() {
		return omoins;
	}

	public int getOplus() {
		return oplus;
	}

	public int getIplus() {
		return iplus;
	}

	public int getC() {
		return c;
	}

	public int getV() {
		return v;
	}

	public int getW() {
		return w;
	}

	/*
	 * Fonctions permettant d'affecter une la valeur aux parametres de la
	 * station
	 */
	public void setNumber(int n) {
		number = n;
	}

	public void setDisplay(boolean a) {
		display = a;
	}

	public void setName(String s) {
		name = s;
	}

	public void setAdress(String s) {
		adress = s;
	}

	public void setLat(double f) {
		latitude = f;
	}

	public void setLng(double f) {
		longitude = f;
	}

	public void setBanking(boolean b) {
		banking = b;
	}

	public void setBonus(boolean b) {
		bonus = b;
	}

	public void setStatus(String s) {
		status = s;
	}

	public void setContract(String s) {
		contract = s;
	}

	public void setK(int i) {
		k = i;
	}

	public void setKsi(int num, int i) {
		ksi.put(num, i);
	}

	public void setBeta(int num, int i) {
		beta.put(num, i);
	}

	public void setImoins(int num, int i) {
		imoins.put(num, Math.max(0, i));
	}

	public void setX(int i) {
		x = i;
	}

	public void setABS(int i) {
		abs = i;
	}

	public void setTS(long s) {
		ts = new Timestamp(s);
	}


	public void setOplus(int i) {
		oplus = i;
	}

	public void setIplus(int i) {
		iplus = Math.max(0, i);
	}

	public void setC(int i) {
		c = i;
	}

	public void setV(int i) {
		v = i;
	}

	public void setW(int i) {
		w = i;
	}

	public void setOmoins(int i) {
		if (i > 0) {
			omoins = i;
			setOplus(0);
		} else {
			omoins = 0;
			setOplus(-i);
		}
	}
	
	public void affichage() {
		System.out.println("Station_" + number + " :" + "\n\tk = " + k + ",\t\tX = " + x);
		System.out.println("\tOplus = " + oplus + ",\tIplus = " + iplus);
//		affichageKsi();
	}
	
	public void affichageKsi(){
		int count = 0;
		for (int j = 0; j < ksi.size(); j++) {
			if(j == number)
				count ++;
			
			System.out.print("\tksi_" + number + "" + count + " = " + ksi.get(count));
			count++;
		}
		System.out.println("\n");
	}
}

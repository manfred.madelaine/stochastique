package stocka;
import java.util.Scanner;

import javax.swing.JFileChooser;

public class OpenFile {
	JFileChooser fileChooser = new JFileChooser();
	StringBuilder sb = new StringBuilder();
	
	public void PickFile() throws Exception {
		if(fileChooser.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) {
			java.io.File file = fileChooser.getSelectedFile();
			Scanner input = new Scanner(file);
			input.close();
		}
		else {
			sb.append("No file was selected");
		}
	}
}

package stocka;

import java.util.Random;

/**
 * 
 * @author Manfred, Ismail, Saad, Charles
 *
 */
public class Echantillon {
	private int nb_scenarios;
	private Scenario[] scenarios;

	/**
	 * Constructeur de la classe Echantillon: cree des echantillons de scenarios
	 * a partir d'un scenario de reference
	 * 
	 * @param x
	 *            : taille de l'echantillon, cad, le nombre de scenarios le composant
	 * @param s_initial
	 *            : scenario de reference
	 * @param modifier_val_de_x : booleen indiquant si l'on doit changer la valeur de x pour chacunes des stations crees
	 * ou s'il faut juste affecte a ce nouveau x, le x correspondant dans le scenario de reference
	 */
	public Echantillon(int x, Scenario s_initial, boolean modifier_val_de_x) {
		this.nb_scenarios = x;
		scenarios = new Scenario[nb_scenarios];

		System.out.println("Initialisation d'un echantillon compose de "+ nb_scenarios + " scenario(s)");
		for (int i = 0; i < nb_scenarios; i++) {
			scenarios[i] = createScenario(s_initial, modifier_val_de_x);
		}
	}

	/**
	 * Fonction permettant de creer un scenario a partir d'un scenario de reference.
	 * elle fait tire aléatoirement une demande pour le nouveau scenario dans l'interval suivant:
	 * [-20%(demande initial) ; 20%(demande initial)].
	 * 
	 * @param s_initial : scenario de reference
	 * @return le nouveau scenario
	 */
	public Scenario createScenario(Scenario s_initial, boolean modifier_val_de_x) {		
		Scenario sc = new Scenario(s_initial.getTemperature(), s_initial.getProbabilite(), s_initial.getStations());
		int new_ksi;
		int old_ksi;
		int borne_inf;
		int borne_sup;
		Random r = new Random();

		for (int i = 0; i < sc.getStations().size(); i++) {
			if(modifier_val_de_x)
				sc.getStations().get(i).setX(sc.getStations().get(i).getK()/10);
			else
				sc.getStations().get(i).setX(s_initial.getStations().get(i).getX());
			
			sc.getStations().get(i).setABS(sc.getStations().get(i).getK() - sc.getStations().get(i).getX());
			
    		for (Station ss : sc.getStations()) {
    			if (sc.getStations().get(i) != ss){
					old_ksi = sc.getStations().get(i).getKsi(ss.getNumber());
					if(old_ksi == 0 )
						sc.getStations().get(i).setKsi(ss.getNumber(), old_ksi);
						
					else{
						borne_inf = (int) (old_ksi * (-0.2)) + old_ksi;
						borne_sup = (int) (old_ksi * ( 0.2)) + old_ksi;
						if(borne_sup <= borne_inf)
							borne_inf -= 1;
						
						new_ksi = r.nextInt(borne_sup - borne_inf + 1) + borne_inf ;
						sc.getStations().get(i).setKsi(ss.getNumber(), new_ksi);
					}
    			}
    		}
		}
		return sc;
	}

	public int getNb_scenarios() {
		return nb_scenarios;
	}

	public void setNb_scenarios(int nb_scenarios) {
		this.nb_scenarios = nb_scenarios;
	}

	public Scenario[] getScenarios() {
		return scenarios;
	}

	public void setScenarios(Scenario[] scenarios) {
		this.scenarios = scenarios;
	}
	
}

package stocka;

import java.awt.Canvas;
import java.awt.Color;
import java.awt.Event;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.KeyEvent;
import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.Random;

import javax.swing.JFrame;
import javax.swing.KeyStroke;

public class RenderCanvas extends Canvas {
	public Mousecontrol s;
	private ArrayList<Station> station;
	private int zoom = 100;
	private static final int    EARTH_RADIUS = 6371;
	private static final double FOCAL_LENGTH = 500;
	
	
	public RenderCanvas(ArrayList<Station> station) {
		this.station = station;
		s = new Mousecontrol(this);
		this.addMouseListener(s);
		this.addMouseMotionListener(s);
		coordonnees();
	}
	
	public ArrayList<Station> getStations() {
		return station;
	}
	
	public void setStations(ArrayList<Station> a) {
		station = a;
		coordonnees();
	}
	
	public int getZoom() {
		return zoom;
	}
	
	public void setZoom(int i) {
		zoom = i;
	}
	
	public void coordonnees() {
		for(Station s : station) {
			Random rnd = new Random();
			double latitude = (s.getLat() + rnd.nextDouble())*1000.0;
            double longitude = (s.getLng() + rnd.nextDouble())*1000.0;

            double x = EARTH_RADIUS * Math.sin(latitude) * Math.cos(longitude);
            double y = EARTH_RADIUS * Math.sin(latitude) * Math.sin(longitude);
            double z = EARTH_RADIUS * Math.cos(longitude);

            double projectedX = x * FOCAL_LENGTH / (FOCAL_LENGTH + z);
            double projectedY = y * FOCAL_LENGTH / (FOCAL_LENGTH + z);
            s.setLat(projectedX);
	        s.setLng(projectedY);
		}
	}
	
	public void paint(Graphics gg) {	
		super.paint(gg);
		Graphics2D g = (Graphics2D) gg;
		g.setColor(Color.BLUE);
		g.drawString("X", 10, 20);	
		g.drawLine(20, 30, 20, 579);		
		g.drawString("Y", 510, 590);
		g.drawLine(20, 579, 510, 579);
		for (Station s : station) {
			int x = (int)s.getLat();
			int y = (int)s.getLng();
			if (s.getDisplay()) {
				g.setColor(Color.black);
				g.drawLine((zoom*x/100)+5, (zoom*y/100), (zoom*x/100)+5, (zoom*y/100)-10);
				g.drawRect((zoom*x/100)-50, (zoom*y/100)-70, 110, 60);
				g.fillRect((zoom*x/100)-50, (zoom*y/100)-70, 110, 60);
				g.setColor(Color.white);
				g.drawString("x : "+s.getX()+" velos", (zoom*x/100)-40, (zoom*y/100)-55);
				g.drawString("I+ : "+s.getIplus()+" velos", (zoom*x/100)-40, (zoom*y/100)-40);
				g.drawString("c : "+s.getC()+" / v : "+s.getV(), (zoom*x/100)-40, (zoom*y/100)-26);
				g.drawString("w : "+s.getW()+" / k : "+s.getK(), (zoom*x/100)-40, (zoom*y/100)-14);
			}
			g.setColor(Color.black);
			g.drawOval((zoom*x/100), (zoom*y/100), 10, 10);
			if (s.getDisplay()) {
				g.setColor(Color.BLUE);
				g.fillOval((zoom*x/100), (zoom*y/100), 10, 10);
			}
		}
	}
}

package stocka;
import javax.swing.JFrame;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;

import java.awt.BorderLayout;
import java.awt.Color;

import javax.swing.JSplitPane;
import javax.swing.JTextArea;

import java.awt.Dimension;
import javax.swing.JTextField;
import javax.swing.JLayeredPane;
import javax.swing.JScrollPane;
import javax.swing.JLabel;
import com.jgoodies.forms.factories.DefaultComponentFactory;
import javax.swing.JSlider;
import javax.swing.JSeparator;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.awt.event.ActionEvent;
import javax.swing.SwingConstants;
import java.awt.Insets;

public class UserInterface extends JFrame{

	private JTextField textField;
	private JTextField textField_2;
	private JTextField textField_1;
	private RenderCanvas render;
	private DefaultComboBoxModel<String> model;
	private JComboBox<String> listStation;
	private Scenario res;
	
	public RenderCanvas getCanvas() {
		return render;
	}
	
	/**
	 * Create the application.
	 */
	public UserInterface(ArrayList<Station> s) {
		render = new RenderCanvas(s);
		render.setBackground(Color.white);
		initialize();		
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		this.setTitle("Projet Stochastique");
		this.setBounds(100, 100, 1025, 640);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		JSplitPane splitPane = new JSplitPane();
		splitPane.setResizeWeight(0.3);
		splitPane.setPreferredSize(new Dimension(179, 25));
		this.getContentPane().add(splitPane, BorderLayout.CENTER);
		
		/**
		 * GUI left part
		 */
		JLayeredPane layeredPane_gauche = new JLayeredPane();
		splitPane.setLeftComponent(layeredPane_gauche);

		
		JLabel lblTitre = DefaultComponentFactory.getInstance().createLabel("Projet Stochastique");
		lblTitre.setBounds(90, 30, 150, 14);
		layeredPane_gauche.add(lblTitre);
		
		JSeparator separator = new JSeparator();
		separator.setBounds(64, 74, 155, 8);
		layeredPane_gauche.add(separator);
		
		JLabel lblRemplissez = DefaultComponentFactory.getInstance().createLabel("1 - Remplissez les param\u00E8tres d\u00E9terministes");
		lblRemplissez.setBounds(10, 103, 259, 14);
		layeredPane_gauche.add(lblRemplissez);
		
		JLabel lblidentiquePourLensemble = DefaultComponentFactory.getInstance().createTitle("( identique pour l'ensemble des stations )");
		lblidentiquePourLensemble.setBounds(27, 112, 242, 23);
		layeredPane_gauche.add(lblidentiquePourLensemble);
		
		JLabel lblC = DefaultComponentFactory.getInstance().createTitle("c");
		lblC.setBounds(46, 147, 17, 14);
		layeredPane_gauche.add(lblC);
		
		JLabel lblV = DefaultComponentFactory.getInstance().createTitle("v");
		lblV.setBounds(148, 147, 17, 14);
		layeredPane_gauche.add(lblV);
		
		JLabel lblW = DefaultComponentFactory.getInstance().createTitle("w");
		lblW.setBounds(46, 180, 17, 14);
		layeredPane_gauche.add(lblW);
		
		textField = new JTextField();
		textField.setBounds(63, 147, 44, 20);
		layeredPane_gauche.add(textField);
		textField.setColumns(10);
		
		textField_2 = new JTextField();
		textField_2.setBounds(63, 177, 44, 20);
		layeredPane_gauche.add(textField_2);
		textField_2.setColumns(10);
		
		textField_1 = new JTextField();
		textField_1.setBounds(168, 147, 44, 20);
		layeredPane_gauche.add(textField_1);
		textField_1.setColumns(10);
		
		JSeparator separator_1 = new JSeparator();
		separator_1.setBounds(64, 236, 155, 8);
		layeredPane_gauche.add(separator_1);
		
		JLabel lblLancer = DefaultComponentFactory.getInstance().createLabel("2 - Lancer au choix une des options suivantes :");
		lblLancer.setBounds(10, 265, 281, 14);
		layeredPane_gauche.add(lblLancer);
		
		JLabel lblA = DefaultComponentFactory.getInstance().createTitle("a)");
		lblA.setBounds(10, 314, 17, 14);
		layeredPane_gauche.add(lblA);
		
		JButton btnRecuitDterministe = new JButton("Recuit d\u00E9terministe");
		btnRecuitDterministe.setBounds(36, 312, 200, 23);
		layeredPane_gauche.add(btnRecuitDterministe);
		btnRecuitDterministe.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent a) {
				int c = Integer.parseInt(textField.getText());
				int v = Integer.parseInt(textField_1.getText());
				int w = Integer.parseInt(textField_2.getText());
				res = Main.test1(c,v,w);
				DefaultComboBoxModel<String> m = new DefaultComboBoxModel<String>();
				for (Station s : res.getStations()) {
					System.out.println(s.getName());
					m.addElement(s.getName());
				}
				listStation.setModel(m);
				render.setStations(res.getStations());
				render.repaint();		}
			});
		
		JLabel lblBNombreDe = DefaultComponentFactory.getInstance().createTitle("b)");
		lblBNombreDe.setBounds(10, 360, 17, 14);
		layeredPane_gauche.add(lblBNombreDe);
		
		JButton button = new JButton("Recuit stochastique");
		button.setBounds(36, 360, 200, 23);
		layeredPane_gauche.add(button);
		
		JLabel lblC_1 = DefaultComponentFactory.getInstance().createTitle("c)");
		lblC_1.setBounds(10, 406, 17, 14);
		layeredPane_gauche.add(lblC_1);
		
		JButton btnRecuitStochastique = new JButton("SAA");
		btnRecuitStochastique.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				int c = Integer.parseInt(textField.getText());
				int v = Integer.parseInt(textField_1.getText());
				int w = Integer.parseInt(textField_2.getText());
				Main.testSAA(c,v,w);
			}
		});
		btnRecuitStochastique.setBounds(36, 406, 200, 23);
		layeredPane_gauche.add(btnRecuitStochastique);
		
		JSeparator separator_2 = new JSeparator();
		separator_2.setBounds(63, 470, 155, 8);
		layeredPane_gauche.add(separator_2);
		
		JLabel lblTlcharger = DefaultComponentFactory.getInstance().createLabel("3 - T\u00E9l\u00E9charger les r\u00E9sultats");
		lblTlcharger.setBounds(10, 500, 268, 14);
		layeredPane_gauche.add(lblTlcharger);
		
		JButton btnTlchargerLeFichier = new JButton("T\u00E9l\u00E9charger les r\u00E9sultats finaux");
		btnTlchargerLeFichier.setBounds(36, 530, 242, 23);
		layeredPane_gauche.add(btnTlchargerLeFichier);	
		
		/**
		 * GUI right part
		 */
		JLayeredPane layeredPane_droite = new JLayeredPane();
		splitPane.setRightComponent(layeredPane_droite);
		
		render.setSize(560, 600);
		layeredPane_droite.add(render, new Integer(0));
		
		JButton btnActiverLesLiens = new JButton("Activer les liens");
		btnActiverLesLiens.setBounds(566, 11, 125, 20);
		layeredPane_droite.add(btnActiverLesLiens);
		
		JLabel labelStation = DefaultComponentFactory.getInstance().createTitle("Choisir une station");
		labelStation.setBounds(566, 50, 125, 20);
		layeredPane_droite.add(labelStation);
		
		model = new DefaultComboBoxModel<String>();
		listStation = new JComboBox<String>(model);
		for (Station s : render.getStations())
			model.addElement(s.getName());
		listStation.setBounds(566, 70, 125, 20);
		layeredPane_droite.add(listStation);
		JTextArea textArea = new JTextArea();
		textArea.setBounds(566, 90, 130, 500);
		JScrollPane pane = new JScrollPane(textArea);
		pane.setBounds(566, 90, 130, 500);
		layeredPane_droite.add(pane);
		
		listStation.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent a) {
				JComboBox<String> combo = (JComboBox<String>)a.getSource();
                String current = (String)combo.getSelectedItem();
                Station b = null;
                textArea.setText("");
				for (Station s : getCanvas().getStations())
					if (s.getName().equals(current))
						b = s;
				for (int i : b.getBeta().keySet()) {
					textArea.setText(textArea.getText()+"\n"+"B"+b.getNumber()+"/"+i+" : "+b.getBeta(i));
					textArea.setText(textArea.getText()+"\n"+"I-"+b.getNumber()+"/"+i+" : "+b.getImoins(i));
				}
			}	
		});
		
	}
}